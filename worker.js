require('dotenv').config({ path: '.env' });
const { exec } = require('child_process')
const mongoose = require('mongoose');
const async = require('async');

const DB_CONNECTION_URL = process.env.DB_CONNECTION_URL;
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
// Services
const articleHandler = require('./dbHandlers/article');
const transtionVendor = require('./vendors/translation')
const rabbitmqService = require('./vendors/rabbitmq');
const queues = require('./vendors/rabbitmq/queues');
const { TRANSLATE_ARTICLE_TEXT_QUEUE, TRANSLATE_ARTICLE_TEXT_FINISH_QUEUE } = queues;


mongoose.connect(DB_CONNECTION_URL) // connect to our mongoDB database //TODO: !AA: Secure the DB with authentication keys

let channel;
rabbitmqService.createChannel(RABBITMQ_SERVER, (err, ch) => {
    if (err) throw err;
    channel = ch;
    channel.prefetch(1)
    channel.consume(TRANSLATE_ARTICLE_TEXT_QUEUE, onTranslateArticleText, { noAck: false })
})


function onTranslateArticleText(msg) {
    const { articleId, lang } = JSON.parse(msg.content.toString());
    let article;
    console.log('translate article request', articleId, lang)
    articleHandler.findById(articleId)
    .then((articleDoc) => {
        if (!articleDoc) throw new Error('Invalid article id');
        article = articleDoc.toObject()
        return new Promise((resolve, reject) => {
            article.articleType = 'translation';
            const translationFuncArray = [];
            let totalTranslateCount = article.slides.reduce((acc, slide) => acc + slide.content.filter((c) => c.text.trim().length > 0).length, 0);
            let doneCount = 0;
            article.slides.forEach((slide) => {
                slide.content.forEach((subslide) => {
                    if (subslide.speakerProfile && subslide.speakerProfile.speakerNumber === -1) {
                    } else {
                        subslide.audio = '';
                    }
                    if (subslide.text && subslide.text.trim().length > 0) {
                        translationFuncArray.push((cb) => {
                            transtionVendor.google.translateText(subslide.text, lang)
                            .then((translatedText) => {
                                doneCount ++;
                                subslide.text = translatedText;
                                updateTranslationProgress(articleId, Math.floor(doneCount/totalTranslateCount * 100), () => {
                                    cb();
                                });
                            })
                            .catch(err => {
                                console.log('error translating subslide', err);
                                subslide.text = '';
                                cb();
                            })
                        })
                    } else {
                        subslide.text = '';
                    }
                })
            });

            async.series(translationFuncArray, (err) => {
                if (err) {
                    console.log('error translating', err);
                }
                resolve(article)
            })
        })
        .then((article) => {
            articleHandler.updateById(articleId, { slides: article.slides, langCode: lang, translationProgress: 100 })
            .then(() => {
                updateTranslationProgress(articleId, 100, () => {
                    channel.ack(msg)
                    channel.sendToQueue(TRANSLATE_ARTICLE_TEXT_FINISH_QUEUE, new Buffer(JSON.stringify({ articleId, lang, success: true })));
                });
            })
            .catch((err) => {throw err})
        })
        .catch((err) => {
            console.log('Errror translating article', err);
            article.slides.forEach((slide) => {
                slide.content.forEach((subslide) => {
                    subslide.audio = '';
                    subslide.text = '';
                })
            })
            articleHandler.updateById(articleId, { slides: article.slides, langCode: lang, translationProgress: 100 })
            .then(() => {
                updateTranslationProgress(articleId, 100, () => {
                    channel.ack(msg);
                    channel.sendToQueue(TRANSLATE_ARTICLE_TEXT_FINISH_QUEUE, new Buffer(JSON.stringify({ articleId, lang, success: false })));
                });
            })
            .catch((err) => {
                console.log('error updating slides', err);
                updateTranslationProgress(articleId, 100, () => {
                    channel.sendToQueue(TRANSLATE_ARTICLE_TEXT_FINISH_QUEUE, new Buffer(JSON.stringify({ articleId, lang, success: false })));
                    channel.ack(msg)
                });
            })
        })

    })
    .catch((err) => {
        console.log(err);
        updateTranslationProgress(articleId, 100, () => {
            channel.ack(msg)
        });
    })
}

function updateTranslationProgress(articleId, translationProgress, callback = () => {}) {
    articleHandler.updateById(articleId, { translationProgress })
    .then(() => {
        console.log('updated progress', translationProgress)
        callback();
    })
    .catch((err) => {
        console.log('error updating progress', err);
        callback(err);
    })
}