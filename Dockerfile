FROM node:10.15.0-jessie

WORKDIR /tvw_translator

COPY package*.json ./
RUN npm install

COPY . .
RUN echo "GOOGLE_APPLICATION_CREDENTIALS=/tvw_translator/gsc_creds.json" >> .env

# ADD AWS CREDENTIALS FILE
ARG AWS_KEYS_FILE_BASE64
RUN mkdir ~/.aws
RUN echo ${AWS_KEYS_FILE_BASE64} | base64 --decode > ~/.aws/credentials

CMD ["npm", "run", "docker:prod"]
